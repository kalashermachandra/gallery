#pragma once
#include "IDataAccess.h"


class DB: public IDataAccess
{
public:
	DB();
	~DB();

	// album related
	virtual const std::list<Album> getAlbums(); //
	virtual const std::list<Album> getAlbumsOfUser(const User & user); // 
	virtual void createAlbum(const Album & album); //
	virtual void deleteAlbum(const std::string & albumName, int userId); //
	virtual bool doesAlbumExists(const std::string & albumName, int userId); //
	virtual Album openAlbum(const std::string & albumName); //
	virtual void closeAlbum(Album & pAlbum); // 
	virtual void printAlbums(); //

	// picture related
	virtual const std::list<Picture> getPictures(); //
	virtual void addPictureToAlbumByName(const std::string & albumName, const Picture & picture); //
	virtual void removePictureFromAlbumByName(const std::string & albumName, const std::string & pictureName); //
	virtual void tagUserInPicture(const std::string & albumName, std::string & pictureName, int userId); //
	virtual void untagUserInPicture(const std::string & albumName, const std::string & pictureName, int userId); //

	// user related
	virtual const std::list<User> getUsers(); //
	virtual void printUsers(); //
	virtual User getUser(int userId); //
	virtual void createUser(User & user); //
	virtual void deleteUser(const User & user); //
	virtual bool doesUserExists(int userId); //


	// user statistics
	virtual int countAlbumsOwnedOfUser(const User & user); //
	virtual int countAlbumsTaggedOfUser(const User & user); //
	virtual int countTagsOfUser(const User & user); //
	virtual float averageTagsPerAlbumOfUser(const User & user); //
	int countTagsOfUserPerAlbum(const User& user, const  Album& album); //

	// queries
	virtual User getTopTaggedUser(); //
	virtual Picture getTopTaggedPicture(); //
	virtual std::list<Picture> getTaggedPicturesOfUser(const User & user); //

	virtual bool open(); //
	virtual void close(); //
	virtual void clear(); //
private:
	//
	static int getUserToList(void* data, int argc, char** argv, char** azColName);
	static int getPictureToList(void* data, int argc, char** argv, char** azColName);
	static int getAlbumToList(void* data, int argc, char** argv, char** azColName);
	static int doesExistCALLBACK(void* data, int argc, char** argv, char** azColName);
	static int LastUserIdCALLBACK(void* data, int argc, char** argv, char** azColName);
	static int averageTagsCALLBACK(void* data, int argc, char** argv, char** azColName);
	static int deleteuserCALLBACK(void* data, int argc, char** argv, char** azColName);
	static int addPictureToAlbumByNameCALLBACK(void* data, int argc, char** argv, char** azColName);
	static int deletePicturecallback(void* data, int argc, char** argv, char** azColName);
	static int deleteTagscallback(void* data, int argc, char** argv, char** azColName);
	void init();
	
	void updateLists();
	//
};
