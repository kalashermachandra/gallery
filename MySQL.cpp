#include "MySQL.h"
#include <sstream>
#include "MyException.h"


sqlite3* db;
std::string albumID;
int lastId;

std::list<Album> m_albums;
std::list<User> m_users;
std::list<Picture> m_pictures;

DB::DB()
{
}

DB::~DB()
{
}

int DB::countAlbumsOwnedOfUser(const User& user)
{
	int count = 0;
	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		if (i->getOwnerId() == user.getId())
		{
			count++;
		}
	}
	return count;
}

int DB::countAlbumsTaggedOfUser(const User& user)
{
	int count = 0;
	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		for (auto j = i->getPictures().begin(); j != i->getPictures().end(); j++)
		{
			if (j->isUserTagged(user.getId()))
			{
				count++;
			}
		}
	}
	return count;
}

int DB::countTagsOfUser(const User& user)
{
	int count = 0;
	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		count += countAlbumsTaggedOfUser(user);
	}
	return count;
}

float DB::averageTagsPerAlbumOfUser(const User& user)
{
	float avg = 0.0;
	int count = 0;
	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		avg += this->countTagsOfUserPerAlbum(user,i.operator*());
		count++;
	}
	return avg / count;
}

int DB::countTagsOfUserPerAlbum(const User& user, const Album& album)
{
	int count = 0;
	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		for (auto j = i->getPictures().begin(); j != i->getPictures().begin(); j++)
		{
			for (auto x = j->getUserTags().begin(); x != j->getUserTags().end(); x++)
			{
				if (x.operator*() == user.getId())
				{
					count++;
				}
			}
		}
	}
	return count;
}

User DB::getTopTaggedUser()
{
	int max = 0;
	int maxID = 0, count = 1;
	for (auto i = m_users.begin(); i != m_users.end(); i++)
	{
		int x = this->countTagsOfUser(i.operator*());
		if (x > max)
		{
			max = x;
			maxID = count;
		}
		count++;
	}

	count = 0;
	for (auto i = m_users.begin(); i != m_users.end(); i++)
	{
		if (maxID == count)
		{
			return i.operator*();
		}
		count++;
	}
}

Picture DB::getTopTaggedPicture()
{
	Picture maxPic(1,"");
	int max = 0;
	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		for (auto j = i->getPictures().begin(); j != i->getPictures().end(); j++)
		{
			int num = j->getTagsCount();
			if (max < num)
			{
				max = num;
				maxPic = j.operator*();
			}
		}
	}

	return maxPic;
}

std::list<Picture> DB::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> lst;
	for (auto i = m_pictures.begin(); i != m_pictures.end(); i++)
	{
		if (i->isUserTagged(user.getId()))
		{
			lst.push_back(i.operator*());
		}
	}
	return lst;
}

bool DB::open()
{
	db;
	std::string dbFileName = "galleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK)
	{
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return -1;
	}
	if (doesFileExist == -1)
	{
		init();
	}
	return true;
}

void DB::close()
{
	sqlite3_close(db);
	db = nullptr;
}

void DB::clear()
{
	m_albums.clear();
	m_users.clear();
	m_pictures.clear();
}

const std::list<Album> DB::getAlbums()
{
	std::string sqlStatement = "SELECT MAX(ID) FROM ALBUMS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);

	for (size_t i = 1; i <= lastId; i++)
	{
		std::string sqlStatement = "SELECT * FROM ALBUMS";
		sqlStatement += "WHERE ID = " + std::to_string(lastId) + ";";
		char* errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), getAlbumToList, nullptr, &errMessage);
	}
	return m_albums;
}

const std::list<Album> DB::getAlbumsOfUser(const User& user)
{
	std::list<Album> lst;
	
	for (auto j = m_albums.begin(); j != m_albums.end(); j++)
	{
		if (j->getOwnerId() == user.getId())
		{
			lst.push_back(j.operator*());
		}
	}
	return lst;
}

void DB::createAlbum(const Album& album)
{
	std::string sqlStatement = "SELECT MAX(ID) FROM ALBUMS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);

	sqlStatement = "INSERT INTO ALBUMS VALUES(";
	sqlStatement += std::to_string(lastId + 1) + "," + '"' + album.getName() + '"'  + "," + '"' + album.getCreationDate() + '"' + "," + std::to_string(album.getOwnerId()) + ");";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	lastId++;
	m_albums.push_back(album);
}

void DB::deleteAlbum(const std::string& albumName, int userId)
{


	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE NAME = ";
	sqlStatement += '"' + albumName + '"' + "AND USER_ID = " + std::to_string(userId);
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deletePicturecallback, nullptr, &errMessage);

	sqlStatement = "DELETE FROM ALBUMS WHERE NAME = ";
	sqlStatement += '"' + albumName + '"' + "AND USER_ID = " + std::to_string(userId);
	sqlStatement += ";";

	char* errMessage2 = nullptr;
	int res2 = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage2);
	int i = 1;
	for (auto j = m_albums.begin(); j != m_albums.end(); j++)
	{
		if (i == lastId)
		{
			m_albums.erase(j);
		}
		i++;
	}
}

bool DB::doesAlbumExists(const std::string& albumName, int userId)
{
	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE NAME = ";
	sqlStatement += '"' + albumName + '"' + "AND USER_ID = " + std::to_string(userId);
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), doesExistCALLBACK, nullptr, &errMessage);

	if (lastId == 1)
	{
		return true;
	}
	return false;
}

Album DB::openAlbum(const std::string& albumName)
{
	const char* GetUsers = "SELECT * FROM ALBUMS WHERE NAME ='";
	std::string result = GetUsers + albumName + "';";

	Album a;

	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		if (i->getName() == albumName)
		{
			a = i.operator*();
		}
	}

	return a;
}

void DB::closeAlbum(Album& pAlbum)
{
	//nothing to write here...
}

void DB::printAlbums()
{
	int count = 1;
	for (auto i = m_albums.begin(); i != m_albums.end(); i++)
	{
		std::cout << std::to_string(count) << ". " << i->getName() << ": " << i->getCreationDate() << "\n\t" << "Pictures:";
		std::list<Picture> p = i->getPictures();
		for (auto j = p.begin(); j != p.end(); j++)
		{
			std::cout << j->getName() << std::endl;
		}
		count++;
	}
}

const std::list<Picture> DB::getPictures()
{
	std::string sqlStatement = "SELECT MAX(ID) FROM PICTURES;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);

	for (size_t i = 1; i <= lastId; i++)
	{
		std::string sqlStatement = "SELECT * FROM PICTURES";
		sqlStatement += "WHERE ID = " + std::to_string(lastId) + ";";
		char* errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), getPictureToList, nullptr, &errMessage);
	}
	return m_pictures;
}

void DB::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE NAME = ";
	sqlStatement += '"' + albumName + '"' + ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), addPictureToAlbumByNameCALLBACK, nullptr, &errMessage);
	
	sqlStatement = "SELECT MAX(ID) FROM PICTURES;";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);

	sqlStatement = "INSERT INTO PICTURES VALUES(";
	sqlStatement += std::to_string(lastId + 1) + "," + '"' + picture.getName() + '"' + "," + '"' + picture.getPath() + '"' + "," + '"' + picture.getCreationDate() + '"' + "," + albumID + ");";
	char* errMessage2 = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage2);
	m_pictures.push_back(picture);
}

void DB::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	std::string sqlStatement = "SELECT ID FROM PICTURES WHERE NAME = ";
	sqlStatement += '"' + pictureName + '"';
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deleteTagscallback, nullptr, &errMessage);

	sqlStatement = "DELETE FROM PICTURES WHERE ID = ";
	sqlStatement += albumID;
	sqlStatement += ";";

	char* errMessage2 = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage2);

	int count = 1;
	for (auto i = m_pictures.begin(); i != m_pictures.end(); i++)
	{
		if (count == std::stoi(albumID))
		{
			m_pictures.erase(i);
		}
		count++;
	}
}

void DB::tagUserInPicture(const std::string& albumName, std::string& pictureName, int userId)
{
	std::string sqlStatement = "SELECT ID FROM PICTURES WHERE NAME = ";
	sqlStatement += '"' + pictureName + '"';
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);

	int picId = lastId;

	sqlStatement = "SELECT MAX(ID) FROM TAGS;";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);

	sqlStatement = "INSERT INTO USERS VALUES(";
	sqlStatement += std::to_string(lastId + 1) + "," + std::to_string(picId) + "," + std::to_string(userId) + ");";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

	for (auto i = m_pictures.begin(); i != m_pictures.end(); i++)
	{
		if (i->getName() == pictureName)
		{
			i.operator*().tagUser(userId);
		}
	}
}

void DB::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE NAME = ";
	sqlStatement += '"' + albumName + '"';
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deleteTagscallback, nullptr , &errMessage);
}

const std::list<User> DB::getUsers()
{
	std::string sqlStatement = "SELECT MAX(ID) FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);

	for (size_t i = 1; i <= lastId; i++)
	{
		std::string sqlStatement = "SELECT * FROM USERS";
		sqlStatement += "WHERE ID = " + std::to_string(lastId) + ";";
		char* errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), getUserToList, nullptr, &errMessage);
	}
	return m_users;
}

void DB::printUsers()
{
	for (auto i = m_users.begin(); i != m_users.end(); i++)
	{
		std::cout << i->getId() << ". " << i->getName() << ": ";
		this->printAlbums();
		std::cout << "\n\n";

	}
}

User DB::getUser(int userId)
{
	for (auto i = m_users.begin(); i != m_users.end(); i++)
	{
		if (i->getId() == userId)
		{
			return i.operator*();
		}
	}
	return User(0,"");
}

void DB::createUser(User& user)
{
	std::string sqlStatement = "SELECT MAX(ID) FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), LastUserIdCALLBACK, nullptr, &errMessage);


	sqlStatement = "INSERT INTO USERS VALUES("; 
	sqlStatement += std::to_string(lastId + 1) + "," +  '"' + user.getName() + '"' + ");";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	m_users.push_back(user);
	user.setId(lastId + 1);
}

void DB::deleteUser(const User& user)
{
	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE USER_ID =";
	sqlStatement += user.getId();
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deleteuserCALLBACK, nullptr, &errMessage);

	sqlStatement = "DELETE FROM USERS WHERE ID = ";
	sqlStatement += user.getId();
	sqlStatement += ";";

	char* errMessage2 = nullptr;
	int res2 = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage2);
	
	int count = 1;
	for (auto i = m_users.begin(); i != m_users.end(); i++)
	{
		if (count == user.getId())
		{
			m_users.erase(i);
		}
		count++;
	}
}

bool DB::doesUserExists(int userId)
{
	std::string sqlStatement = "SELECT ID FROM USERS WHERE";
	sqlStatement += "ID = " + std::to_string(userId);
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), doesExistCALLBACK, nullptr, &errMessage);

	if (lastId == 1)
	{
		return true;
	}
	return false;
}



int DB::getUserToList(void* data, int argc, char** argv, char** azColName)
{
	User a(1, "");
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID")
		{
			a.setId(std::atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			a.setName(argv[i]);
		}
	}
	m_users.push_back(a);
	return 0;
}

int DB::getPictureToList(void* data, int argc, char** argv, char** azColName)
{
	Picture a(1,"","","");
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID")
		{
			a.setId(std::atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			a.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION") {
			a.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			a.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "ALBUM_ID") {
			a.setAlbumID(std::atoi(argv[i]));
		}
	}
	m_pictures.push_back(a);
	return 0;
}

int DB::getAlbumToList(void* data, int argc, char** argv, char** azColName)
{
	Album a;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "NAME") 
		{
			a.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			a.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID") {
			a.setOwner(std::atoi(argv[i]));
		}
	}
	m_albums.push_back(a);
	return 0;
}

int DB::doesExistCALLBACK(void* data, int argc, char** argv, char** azColName)
{
	char* s = argv[0];
	if (s != NULL)
	{
		lastId = 1;
	}
	else
	{
		lastId = 0;
	}


	return 0;
}

int DB::LastUserIdCALLBACK(void* data, int argc, char** argv, char** azColName)
{

	char * s = argv[0];
	if (s != NULL)
	{
		lastId = std::stoi(s);
	}
	else
	{
		lastId = 0;
	}

	
	return 0;
}

int DB::averageTagsCALLBACK(void* data, int argc, char** argv, char** azColName)
{
	std::string sqlStatement = "SELECT ID FROM PICTURES WHERE NAME = ";
	sqlStatement += '"' +  + '"';
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deleteTagscallback, nullptr, &errMessage);

	return 0;
}

int DB::deleteuserCALLBACK(void* data, int argc, char** argv, char** azColName)
{
	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE ID = ";
	sqlStatement += argv[0];
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deletePicturecallback, nullptr, &errMessage);

	sqlStatement = "DELETE FROM ALBUMS WHERE ID = ";
	sqlStatement += argv[0];
	sqlStatement += ";";

	char* errMessage2 = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage2);
	return 0;
}

int DB::addPictureToAlbumByNameCALLBACK(void* data, int argc, char** argv, char** azColName)
{
	albumID = argv[0];

	return 0;
}

int DB::deletePicturecallback(void* data, int argc, char** argv, char** azColName)
{
	lastId = std::stoi(argv[0]);
	std::string sqlStatement = "SELECT ID FROM PICTURES WHERE ALBUM_ID = ";
	sqlStatement += argv[0];
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deleteTagscallback, nullptr, &errMessage);

	sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_ID = ";
	sqlStatement += argv[0];
	sqlStatement += ";";

	char* errMessage2 = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage2);

	return 0;
}

int DB::deleteTagscallback(void* data, int argc, char** argv, char** azColName)
{
	albumID = argv[0];
	std::string  sqlStatement = "DELETE FROM TAGS WHERE PICTURE_ID = ";
	sqlStatement += argv[0];
	sqlStatement += ";";

	char* errMessage2 = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage2);

	return 0;
}



void DB::init()
{
	//create Users Table:
	const char* UsersTable = "CREATE TABLE Users(" \
		"ID INTEGER PRIMARY KEY AUTOINCREMENT, "\
		"Name text NOT NULL"\
		");";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, UsersTable, nullptr, nullptr, &errMessage);

	//create ALBUMS Table:
	const char* AlbumsTable = "CREATE TABLE Albums("\
		"ID INTEGER PRIMARY KEY AUTOINCREMENT,"\
		"Name text NOT NULL,"\
		"Creation_Date text NOT NULL,"\
		"User_ID integer not null,"\
		"FOREIGN KEY(User_ID) REFERENCES Users(ID) "\
		");";
	char* errMessage1 = nullptr;
	int res1 = sqlite3_exec(db, AlbumsTable, nullptr, nullptr, &errMessage1);

	//create PICTURES Table:
	const char* PicturesTable = "CREATE TABLE Pictures("\
		"ID integer PRIMARY KEY AUTOINCREMENT,"\
		"Name text NOT NULL,"\
		"Location text NOT NULL,"\
		"Creation_Date text NOT NULL,"\
		"Album_ID integer NOT NULL,"\
		"FOREIGN KEY(Album_ID) REFERENCES Albums(ID)"\
		");";
	char* errMessage2 = nullptr;
	int res2 = sqlite3_exec(db, PicturesTable, nullptr, nullptr, &errMessage2);

	//create TAGS Table:
	const char* TagsTable = "CREATE TABLE Tags("\
		"ID INTEGER PRIMARY KEY AUTOINCREMENT,"\
		"Picture_ID integer NOT NULL,"\
		"User_ID integer NOT NULL,"\
		"FOREIGN KEY(Picture_ID) REFERENCES Pictures(ID),"\
		"FOREIGN KEY(User_ID) REFERENCES Users(ID) "\
		");";
	char* errMessage3 = nullptr;
	int res3 = sqlite3_exec(db, TagsTable, nullptr, nullptr, &errMessage3);
	this->updateLists();
}

void DB::updateLists()
{
	m_users = this->getUsers();
	m_albums = this->getAlbums();
	m_pictures = this->getPictures();
}
